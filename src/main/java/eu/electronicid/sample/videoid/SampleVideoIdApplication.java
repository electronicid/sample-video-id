package eu.electronicid.sample.videoid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleVideoIdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleVideoIdApplication.class, args);
	}
}
